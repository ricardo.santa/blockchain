# Block Chain Aplicación Básica
_____________________________________________________________________________________________
Este proyecto toma como base el proyecto iniciado por: 
## What is blockchain? How it is implemented? And how it works?
Please read the [step-by-step implementation tutorial](https://gist.github.com/satwikkansal/4a857cad2797b9d199547a752933a715) to get your answers :)
_____________________________________________________________________________________________

## Mejoras
Este proyecto BC tiene implementado el almacenamiento en archivos txt tanto de las transacciones pendientes como de la cadena de bloques. Esto para prevenir perdidas de datos cuando el proceso se detiene o la máquina se apaga.

## Instrucciones de uso

Clonar el proyecto,

```sh
$ git clone https://gitlab.com/ricardo.santa/blockchain.git
```

Instalar dependencias,

```sh
$ cd blockchain
$ pip install -r requirements.txt
$ pip install markupsafe==2.0.1
```

Iniciar un nodo servidor de la plataforma BlockChain,

```sh
# OSX
$ export FLASK_APP=node_server.py
$ flask run --port 8000
```

```sh
# Windows users can follow this: https://flask.palletsprojects.com/en/1.1.x/cli/#application-discovery
# Windows
$ set FLASK_APP=node_server.py
$ flask run --port 8000
```

En este momento una instancia de la plataforma blockchain ha iniciado y está disponible en localhost en el puerto 8000. Tenga en cuenta que si usa un servidor con dirección IP pública, tendrá acceso a la plataforma desde cualquier lugar de internet.


En otra terminal (o en otro servidor), corra la aplicación frontend,

```sh
$ python run_app.py
```

La aplicación está disponible en el navegador en esta dirección [http://localhost:5000](http://localhost:5000).

Here are a few screenshots

1. Pagina de consulta de las transacciones almacenadas en la cadena

![image.png](https://gitlab.com/ricardo.santa/blockchain/screenshots/1.png)

2. Pagina de consulta de las transacciones almacenadas en la cadena

![image.png](https://gitlab.com/ricardo.santa/blockchain/screenshots/3.png)

3. Imagen del contenido del Archivo de la cadena

![image.png](https://gitlab.com/ricardo.santa/blockchain/screenshots/archivocadena.png)

4. Imagen del contenido de las transacciones no confirmadas

![image.png](https://gitlab.com/ricardo.santa/blockchain/screenshots/archivotransaccionesnoconfirmadas.png)


5. Imagen del código que permite leer y escribir los datos

![image.png](https://gitlab.com/ricardo.santa/blockchain/raw/master/screenshots/codigoleeryescribir.png)



Para registrar nuevos nodos, utilice el endpoint `register_with/`. Tenga en cuenta que deben estar en la misma red o tener direcciones accesibles. 

Por ejemplo,

```sh
# Verifique que la variable FLASK_APP ya contiene el valor node_server.py antes de iniciar los demás nodos
# Nodo 0
$ flask run --port 8000 &
# Los demás nodos
$ flask run --port 8001 &
$ flask run --port 8002 &
```
Si los nodos están en diferentes URLs, todo pueden utilizar el mismo puerto. En este ejemplo se supone que los nodos se están corriendo en la misma máquina.

Los siguientes son los comandos para registrar los demás nodos. En este caso todos los nodos deben estar iniciados.

```sh
curl -X POST \
  http://urlServ1:8001/register_with \
  -H 'Content-Type: application/json' \
  -d '{"node_address": "http://urlServ0:8000"}'
```

```sh
curl -X POST \
  http://urlServ2:8002/register_with \
  -H 'Content-Type: application/json' \
  -d '{"node_address": "http://urlServ0:8000"}'
```
curl -X POST http://192.168.1.171:8000/register_with -H 'Content-Type: application/json' -d '{"node_address": "http://192.168.1.218:8000"}'

Esto permite que el nodo (urlServ0) en el puerto 8000 establezca contacto con los nodos (urlServ0) en el puerto 8001 and urlServ2 en el pueto 8002, esto permite que los nodos participen activamente en la sincronización de la cadena y en procesos de minado posteriores y por su puesto permitan establecer el consenso.

Cada nodo tiene una web app que permite realizar sincronización y consulta de las transacciones validas. Además puede configurarla para que se conecte a cualquiera de los nodos actualizando el campo  CONNECTED_NODE_ADDRESS` en el archivo [views.py](/app/views.py).

Agradecemos a los autores del artículo y código original por sus aportes a este proyecto educativo.