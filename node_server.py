# import module sys to get the type of exception
from asyncio.windows_events import NULL
import sys
from hashlib import sha256
import json
from json import JSONDecoder, JSONEncoder
import time


from flask import Flask, request
import requests

class MyJSONEncoder(JSONEncoder):
        def default(self, o):
            return o.__dict__

# class MyJSONDecoder(JSONDecoder):
#         def default(self, o):
#             return o.__dict__

class Block():
    def __init__(self, index, transactions, timestamp, previous_hash, nonce=0):
        self.index = index
        self.transactions = transactions
        self.timestamp = timestamp
        self.previous_hash = previous_hash
        self.nonce = nonce

    def compute_hash(self):
        """
        A function that return the hash of the block contents.
        """
        block_string = json.dumps(self.__dict__, sort_keys=True)
        return sha256(block_string.encode()).hexdigest()


    # def blocktoJson(self):
    #     return json.dumps(self.__dict__, sort_keys=True)

    # def toJson(self):
    #     return json.dumps(self, default=lambda o: o.__dict__)

# class Cadena(dict):
#     def __init__(self, bloques=[]):
#         self.bloques = bloques

class Blockchain():
    # difficulty of our PoW algorithm
    difficulty = 2
    ###
    def leer_archivo(self, archivo):        
        lista = []
        # lee archivo JSON y retorna 
        # Opening JSON file
        try:
            with open(archivo,'r') as file:
                #f = open(archivo)
                # returns JSON object as 
                # a dictionary
                #print("antes de leer json")
                data = json.load(file)
                #print("data: ",data)
                
                # Iterating through the json
                # list
                #for i in data['emp_details']:
                for i in data:
                    #print("i: ",i)
                    lista.append(i)
                # Closing file
                #f.close()
        except:
            print("error al leer el archivo")
            print(sys.exc_info()[0])

        return lista
    def leer_archivo2(self, archivo):        
        #data = []
        # lee archivo JSON y retorna 
        # Opening JSON file
        # with open(archivo,'r') as file:
        #     try:
        #         #data = json.load(file)
        #         data = file.readlines()
        #         print("Data:",data)
        #         for i in data[2:len(data)-1:]:
        #             #print("I:",i)
        #             lista.append(json.loads(i))
        #     except:
        #         print("no initial data")
        #         print(sys.exc_info()[0], sys.exc_info()[1])

        try:
            with open(archivo,'r') as file:
                #f = open(archivo)
                # returns JSON object as 
                # a dictionary
                #print("antes de leer json")
                data = json.load(file, cls=JSONDecoder, )
                print("data: ",data)
                print("leido")
                
                # Iterating through the json
                # list
                #for i in data['emp_details']:
                #for i in data:
                    #print("i: ",i)
                #    lista.append(i)
                # Closing file
                #f.close()
        except:
            print("error al leer el archivo...")
            print(sys.exc_info()[0], sys.exc_info()[1])

        return data

    def leer_cadena(self):
        # llama a leer archivo y lee la cadena y la inserta en el arreglo
        #generated_blockchain = Blockchain([])
        # ojo
        self.create_genesis_block()
        data = self.leer_archivo2('blockchain.json')
        print("empezamos el for")
        try:
            for idx, block_data in enumerate(data):
                # print("antes del if")
                # print("indice:",idx)
                # print("blockdata:",block_data)
                if idx == 0:
                    # print("en el if por bloque genesis")
                    continue  # skip genesis block
                # print("vamos con los demás")
                block = Block(block_data["index"],
                            block_data["transactions"],
                            block_data["timestamp"],
                            block_data["previous_hash"],
                            block_data["nonce"])
                # print("vamos para la prueba")
                proof = block_data['hash']
                # print("vamos a adicionar el bloque")
                # print("bloque y prueba:", block, proof)
                added = self.add_block(block, proof)
                if not added:
                    #raise Exception("The chain dump is tampered!!")
                    return []
                #print("la nueva cadena: ", generated_blockchain.__o.dict__)
                #return self.chain
        except:
            print("error en el for de los bloques")
            print(sys.exc_info()[0], sys.exc_info()[1])

    def leer_transacciones_no_confirmadas(self):
        #self.unconfirmed_transactions
        lista = self.leer_archivo('noconfirmed.json')
        # borrar contenido del archivo
        archivo = open('noconfirmed.json','w')
        archivo.write('')
        archivo.close() #Cierras el archivo.
        return lista

    def escribir_transacciones_no_confirmadas(self, transaccion):
        #self.unconfirmed_transactions
        with open('noconfirmed.json','a') as file:
            json.dump(transaccion, file, cls=MyJSONEncoder, indent=4)
            # # First we load existing data into a dict.
            # try:
            #     data = json.load(file)
            #     for i in data:
            #         lista.append(i)
            # except:
            #     print("no initial data")
            # # Join new_data with file_data inside emp_details
            # lista.append(transaccion)
            # # Sets file's current position at offset.
            # file.seek(0)
            # # convert back to json.
            # json.dump(lista, file, indent = 4)

    def escribir_genesis_block(self):
        with open('blockchain.json','w') as file:
            file.write("[\n")
            json.dump(self.chain[0], file, cls=MyJSONEncoder, indent=4, separators=(", ", ": "))
            file.write("\n]")

    def escribir_cadena(self, bloque):
        with open('blockchain.json','r+') as file:
            a=file.read()[1:-1]
            #print("tell: ", file.tell())
            file.seek(file.tell()-1,0)
            #print("len:", len(a))
            file.write(",")
            json.dump(bloque, file, cls=MyJSONEncoder, indent=4, separators=(", ", ": "))
            file.write("\n]")
        # borrar las no cofirmadas
        archivo = open('noconfirmed.json','w')
        archivo.write('')
        archivo.close() #Cierras el archivo.

        ###

    def __init__(self, chain=[]):
        #self.unconfirmed_transactions = []
        # load file transaction
        self.unconfirmed_transactions = self.leer_transacciones_no_confirmadas()


        self.chain = []
        # load file chain
        try:
            self.leer_cadena()
        except:
            try:
                # no hay datos almacenados para la cadena o no son legibles
                self.create_genesis_block()
                # almacenar cadena para este caso
                self.escribir_genesis_block()
            except:
                print("error al leer el archivo origen de cadena")
                print(sys.exc_info()[0], sys.exc_info()[1])
            

        # global blockchain
        # blockchain = self.leer_cadena()

    def create_genesis_block(self):
        """
        A function to generate genesis block and appends it to
        the chain. The block has index 0, previous_hash as 0, and
        a valid hash.
        """
        genesis_block = Block(0, [], 0, "0")
        genesis_block.hash = genesis_block.compute_hash()
        self.chain.append(genesis_block)
        # OJO
        # with open('blockchain.json','a') as file:
        #     json.dump(genesis_block, file, cls=MyJSONEncoder, indent=4)

    @property
    def last_block(self):
        # if(len(self.chain)==0):
        #     # adicionar el bloque inicial - esto sucede siempre que no se carga el archivo
        return self.chain[-1]

    def add_block(self, block, proof):
        """
        A function that adds the block to the chain after verification.
        Verification includes:
        * Checking if the proof is valid.
        * The previous_hash referred in the block and the hash of latest block
          in the chain match.
        """
        try:
            print("empezamos las validaciones")
            print("vamos a self.last_block")
            previous_hash = self.last_block.hash
            print("tenemos el previos hash")

            if previous_hash != block.previous_hash:
                return False

            if not Blockchain.is_valid_proof(block, proof):
                return False

            block.hash = proof
            self.chain.append(block)
            # # guardar archivo ojo
            # print("la cadena: ",self.chain[::])
            # with open('blockchain.json','w') as file:
            #     json.dump(self.chain, file, indent=4)
            print("bloqueok")
            return True
        except:
            print("error al ladd:block...")
            print(sys.exc_info()[0], sys.exc_info()[1])

    @staticmethod
    def proof_of_work(block):
        """
        Function that tries different values of nonce to get a hash
        that satisfies our difficulty criteria.
        """
        block.nonce = 0

        computed_hash = block.compute_hash()
        while not computed_hash.startswith('0' * Blockchain.difficulty):
            block.nonce += 1
            computed_hash = block.compute_hash()

        return computed_hash

    def add_new_transaction(self, transaction):
        self.unconfirmed_transactions.append(transaction)
        # save transaction to file
        self.escribir_transacciones_no_confirmadas(transaction)


    @classmethod
    def is_valid_proof(cls, block, block_hash):
        """
        Check if block_hash is valid hash of block and satisfies
        the difficulty criteria.
        """
        return (block_hash.startswith('0' * Blockchain.difficulty) and
                block_hash == block.compute_hash())

    @classmethod
    def check_chain_validity(cls, chain):
        result = True
        previous_hash = "0"

        for block in chain:
            #block_hash = block.hash
            block_hash = block['hash']
            print(block_hash)
            # remove the hash field to recompute the hash again
            # using `compute_hash` method.
            #delattr(block, "hash")
            del block['hash']

            if not cls.is_valid_proof(block, block_hash) or \
                    previous_hash != block.previous_hash:
                result = False
                break

            block.hash, previous_hash = block_hash, block_hash

        return result

    def mine(self):
        """
        This function serves as an interface to add the pending
        transactions to the blockchain by adding them to the block
        and figuring out Proof Of Work.
        """
        if not self.unconfirmed_transactions:
            return False

        last_block = self.last_block

        new_block = Block(index=last_block.index + 1,
                          transactions=self.unconfirmed_transactions,
                          timestamp=time.time(),
                          previous_hash=last_block.hash)

        proof = self.proof_of_work(new_block)
        self.add_block(new_block, proof)
        # add block to file
        
        # ojo
        self.escribir_cadena(new_block)
        self.unconfirmed_transactions = []
        # delete transactions file content

        return True

    # def blocktoJson(self):
    #     return json.dumps(self.__dict__, sort_keys=True)

    # def toJson(self):
    #     return json.dumps(self, default=lambda o: o.__dict__)



app = Flask(__name__)

# the node's copy of blockchain
# cadena = Cadena([])
blockchain = Blockchain([])
# OJO
# if(len(blockchain.chain)==0):
#     blockchain.create_genesis_block()
# cadena=blockchain.chain
# the address to other participating members of the network
peers = set()


# endpoint to submit a new transaction. This will be used by
# our application to add new data (posts) to the blockchain
@app.route('/new_transaction', methods=['POST'])
def new_transaction():
    tx_data = request.get_json()
    required_fields = ["author", "content"]

    for field in required_fields:
        if not tx_data.get(field):
            return "Invalid transaction data", 404

    tx_data["timestamp"] = time.time()

    blockchain.add_new_transaction(tx_data)


    return "Success", 201


# endpoint to return the node's copy of the chain.
# Our application will be using this endpoint to query
# all the posts to display.
@app.route('/chain', methods=['GET'])
def get_chain():
    chain_data = []
    for block in blockchain.chain:
        chain_data.append(block.__dict__)
    return json.dumps({"length": len(chain_data),
                       "chain": chain_data,
                       "peers": list(peers)})


# endpoint to request the node to mine the unconfirmed
# transactions (if any). We'll be using it to initiate
# a command to mine from our application itself.
@app.route('/mine', methods=['GET'])
def mine_unconfirmed_transactions():
    result = blockchain.mine()
    if not result:
        return "No transactions to mine"
    else:
        # Making sure we have the longest chain before announcing to the network
        chain_length = len(blockchain.chain)
        consensus()
        if chain_length == len(blockchain.chain):
            # announce the recently mined block to the network
            announce_new_block(blockchain.last_block)            
        return "Block #{} is mined.".format(blockchain.last_block.index)


# endpoint to add new peers to the network.
@app.route('/register_node', methods=['POST'])
def register_new_peers():
    node_address = request.get_json()["node_address"]
    if not node_address:
        return "Invalid data", 400

    # Add the node to the peer list
    peers.add(node_address)

    # Return the consensus blockchain to the newly registered node
    # so that he can sync
    return get_chain()


@app.route('/register_with', methods=['POST'])
def register_with_existing_node():
    """
    Internally calls the `register_node` endpoint to
    register current node with the node specified in the
    request, and sync the blockchain as well as peer data.
    """
    node_address = request.get_json()["node_address"]
    if not node_address:
        return "Invalid data", 400

    data = {"node_address": request.host_url}
    headers = {'Content-Type': "application/json"}

    # Make a request to register with remote node and obtain information
    response = requests.post(node_address + "/register_node",
                             data=json.dumps(data), headers=headers)

    if response.status_code == 200:
        global blockchain
        global peers
        # update chain and the peers
        chain_dump = response.json()['chain']
        blockchain = create_chain_from_dump(chain_dump)
        peers.update(response.json()['peers'])
        return "Registration successful", 200
    else:
        # if something goes wrong, pass it on to the API response
        return response.content, response.status_code


def create_chain_from_dump(chain_dump):
    generated_blockchain = Blockchain([])
    generated_blockchain.create_genesis_block()
    for idx, block_data in enumerate(chain_dump):
        if idx == 0:
            continue  # skip genesis block
        block = Block(block_data["index"],
                      block_data["transactions"],
                      block_data["timestamp"],
                      block_data["previous_hash"],
                      block_data["nonce"])
        proof = block_data['hash']
        added = generated_blockchain.add_block(block, proof)
        if not added:
            raise Exception("The chain dump is tampered!!")
    return generated_blockchain


# endpoint to add a block mined by someone else to
# the node's chain. The block is first verified by the node
# and then added to the chain.
@app.route('/add_block', methods=['POST'])
def verify_and_add_block():
    block_data = request.get_json()
    block = Block(block_data["index"],
                  block_data["transactions"],
                  block_data["timestamp"],
                  block_data["previous_hash"],
                  block_data["nonce"])

    proof = block_data['hash']
    added = blockchain.add_block(block, proof)

    if not added:
        return "The block was discarded by the node", 400

    return "Block added to the chain", 201


# endpoint to query unconfirmed transactions
@app.route('/pending_tx')
def get_pending_tx():
    return json.dumps(blockchain.unconfirmed_transactions)


def consensus():
    """
    Our naive consnsus algorithm. If a longer valid chain is
    found, our chain is replaced with it.
    """
    global blockchain

    longest_chain = None
    current_len = len(blockchain.chain)

    for node in peers:
        response = requests.get('{}chain'.format(node))
        length = response.json()['length']
        chain = response.json()['chain']
        if length > current_len and blockchain.check_chain_validity(chain):
            current_len = length
            longest_chain = chain

    if longest_chain:
        blockchain = longest_chain
        return True

    return False


def announce_new_block(block):
    """
    A function to announce to the network once a block has been mined.
    Other blocks can simply verify the proof of work and add it to their
    respective chains.
    """
    for peer in peers:
        url = "{}add_block".format(peer)
        headers = {'Content-Type': "application/json"}
        requests.post(url,
                      data=json.dumps(block.__dict__, sort_keys=True),
                      headers=headers)







# Uncomment this line if you want to specify the port number in the code
#app.run(debug=True, port=8000)
